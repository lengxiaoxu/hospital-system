import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// element-plus 入
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import './assets/css/base.scss'
import './components/index'

createApp(App)
.use(store)
.use(router)
.use(ElementPlus)
.mount('#app')
