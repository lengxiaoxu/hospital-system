import Test from './index.vue'
import { App } from 'vue'

const TestComponent = {
    install: function (app:App): void {
        app.component('TestComponent', Test)
    }
}

export default TestComponent