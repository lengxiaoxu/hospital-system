import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import Layout from '@/layout/index.vue'
import StaticRoute from './module/statistic'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: {name: 'Home'}
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/login/Login.vue'),
    meta: {
      title: '登录',
    }
  },
  {
    path: '/home',
    name: 'Home',
    component: Layout,
    children: StaticRoute
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
