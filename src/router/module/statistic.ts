import { RouteRecordRaw } from 'vue-router'

const StaticRoute: Array<RouteRecordRaw> = [
    {
        path: '/static',
        name: 'Static',
        component: () => import('@/views/home/statistic/index.vue'),
        meta: {
            title: '统计',
            slide: true
        }
    },
    {
        path: '/order',
        name: 'Order',
        component: () => import('@/views/home/order/index.vue'),
        meta: {
            title: '开单',
            slide: true
        }
    },
    {
        path: '/medicine-record',
        name: 'MedicineRecord',
        component: () => import('@/views/home/medicine-records/index.vue'),
        meta: {
            title: '药品录入',
            slide: true
        }
    }
]

export default StaticRoute;